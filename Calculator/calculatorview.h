#ifndef CALCULATORVIEW_H
#define CALCULATORVIEW_H

#include <QMainWindow>
#include "calculator.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CalculatorView; }
QT_END_NAMESPACE

class CalculatorView : public QMainWindow
{
    Q_OBJECT

public:
    CalculatorView(QWidget *parent = nullptr);
    ~CalculatorView();

private:
    void appendNumber(const QString& s);
    void appendOperator(const QString& op);
    void clearLastOp();
    void clear();
    void clearE();
    void setError(Calculator::Error err);
    bool isError() const;
    QString getErrorMsg() const;

private slots:
    void on_btnC_clicked();

    void on_btnCE_clicked();

    void on_btnDel_clicked();

    void on_btnEnter_clicked();

    void on_btnNP_clicked();

    void on_btnDot_clicked();

    void on_btn0_clicked();

    void on_btn1_clicked();

    void on_btn2_clicked();

    void on_btn3_clicked();

    void on_btn4_clicked();

    void on_btn5_clicked();

    void on_btn6_clicked();

    void on_btn7_clicked();

    void on_btn8_clicked();

    void on_btn9_clicked();

    void on_btnAdd_clicked();

    void on_btnSub_clicked();

    void on_btnMul_clicked();

    void on_btnDiv_clicked();

private:
    Ui::CalculatorView *ui;
    QString lastNumber;
    QString lastOp;
    QString currNumber = "0";
    bool isInput = false;
    Calculator::Error mErrorCode = Calculator::Error::ERR_OK;

};
#endif // CALCULATORVIEW_H
