#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QObject>

class Calculator
{
public:
    enum class Error {
        ERR_OK = 0,
        ERR_INVALID_EXPR = 1,
        ERR_DIVIDE_ZERO = 2,
        ERR_OVERFLOW = 3,
    };

public:
    Calculator();
    double exec(const QString& expr);
    bool isError() const;
    Error errorCode() const;
    void clearError();

private:
    double strToDouble(const char* p, int& pos);
    QList<QVariant> parseExpression(const QString& expr);
    void setError(Error error);
    double calc(double num1, double num2, const QString& op);
    double add(double num1, double num2);
    double sub(double num1, double num2);
    double mul(double num1, double num2);
    double div(double num1, double num2);

private:
    Error mError = Error::ERR_OK;
};

#endif // CALCULATOR_H
